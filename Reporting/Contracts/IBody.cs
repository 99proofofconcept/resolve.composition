﻿using System;

namespace Reporting.Contracts
{
    public interface IBody
    {
        string Content();
    }
}