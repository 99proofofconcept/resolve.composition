﻿namespace Reporting.Contracts
{
    public interface IFooter
    {
        string Content();
    }
}
