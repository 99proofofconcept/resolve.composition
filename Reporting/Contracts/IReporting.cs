﻿
namespace Reporting.Contracts
{
    public interface IReporting
    {
        string Generate(IBody body);
        
    }
}
