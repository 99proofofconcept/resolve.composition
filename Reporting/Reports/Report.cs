﻿using System;
using System.Text;
using Reporting;
using Reporting.Contracts;
using Reporting.Kernel;

namespace Reporting
{
    public class Report: IReporting
    {
        private readonly IHeader header;
        private readonly IFooter footer;

        protected Report(IHeader header, IFooter footer)
        {
            this.header = header;
            this.footer = footer;
        }

        public virtual string Generate(IBody body)
        {
            StringBuilder sb = new StringBuilder();
            
            if (body == null)
                throw new ArgumentNullException(nameof(body));

            
            sb.Append(header.Content() + " -- ");
            sb.Append(body.Content()   + " -- ");
            sb.Append(footer.Content());

            return sb.ToString();
        }

        public class Resolve<TReporting, THeader, TFooter> : IDisposable
            where TReporting : IReporting
            where THeader : IHeader
            where TFooter : IFooter
        {
            private IResolver container;

            public TReporting Reporting
            {
                get
                {
                    container = new Resolver();

                    container.Register<TReporting, TReporting>();
                    container.Register<IHeader, THeader>();
                    container.Register<IFooter, TFooter>();

                    return container.Resolve<TReporting>();
                }
            }

            #region IDisposable Support
            private bool disposedValue = false;

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        container.Dispose();
                        container = null;
                    }
                    
                    disposedValue = true;
                }
            }
            
            public void Dispose() { Dispose(true); }
            #endregion
        }
    }
}
