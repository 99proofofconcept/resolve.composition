﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Reporting.Kernel
{
    internal class Resolver: IResolver
    {
        private readonly Dictionary<Type, Type> dependencyMap;

        public Resolver() { dependencyMap = new Dictionary<Type, Type>(); }

        public void Register<TFrom, TTo>() { dependencyMap.Add(typeof(TFrom), typeof(TTo)); }

        public T Resolve<T>() { return (T)Resolve(typeof(T)); }

        protected object Resolve(Type type)
        {
            Type resolvedType = default(Type);

            try
            {
                resolvedType = dependencyMap[type];
            }
            catch
            {
                throw new ResolverException($"Could not resolve type { type.FullName } - Description: { nameof(type) }");
            }

            return (!type.IsInterface) ? CreateInstanceWithParameters(resolvedType) : CreateInstanceWithoutParameters(resolvedType);
            
        }

        protected object CreateInstanceWithParameters(Type resolvedType)
        {
            try
            {
                var firstContructor = resolvedType
                                        .GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance)
                                        .First();

                var constructorParameters = firstContructor.GetParameters();

                IList<object> parameters = new List<object>();

                constructorParameters
                    .ToList()
                    .ForEach((ParameterInfo pi) => 
                        {
                            parameters.Add(Resolve(pi.ParameterType));
                        });
                
                return firstContructor?.Invoke(parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw new ResolverException(ex.Message) ;
            }
        }

        protected object CreateInstanceWithoutParameters(Type resolvedType)
        {
            try
            {
                return Activator.CreateInstance(resolvedType, true);
            }
            catch (Exception ex)
            {
                throw new ResolverException(ex.Message);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; 

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    dependencyMap.Clear();
                }
                
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
