﻿using FakeItEasy;
using Reporting.Contracts;
using Xunit;

namespace ReportingTest
{

    public class ContractTest
    {
        [Fact]
        public void Should_get_the_content_of_the_body_type()
        {
            var body = A.Fake<IBody>();
            var initialContent = "This is the content of the boby";
            A.CallTo( () => body.Content()).Returns(initialContent);

            var contentResult = body.Content();
            
            Assert.Equal(initialContent, contentResult); 
        }

        [Fact]
        public void Should_get_the_content_of_the_header_type()
        {
            var header = A.Fake<IHeader>();
            var initialContent = "Header content";
            A.CallTo( () => header.Content()).Returns(initialContent);

            var contentResult = header.Content();

            Assert.Equal(initialContent, contentResult);
        }

        [Fact]
        public void Should_get_the_content_of_the_footer_type()
        {
            var footer = A.Fake<IFooter>();
            var initialContent = "Footer content";
            A.CallTo( () => footer.Content() ).Returns(initialContent);

            var contentResult = footer.Content();

            Assert.Equal(initialContent, contentResult);
        } 
    }
}
